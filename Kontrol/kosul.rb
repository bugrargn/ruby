=begin
grad = false

if grad
    puts "Mezunsun"
else
    puts "Mezun değil"
end

print "İşlemi Giriniz: "

option = gets.chomp() # gets.chomp giriş yapıldığında boşluk iptal eder. 1\n olarak almaz

if option == "1"
    puts "İşlem 1"
elsif option == "2"
    puts "İşlem 2"
elsif option == "3"
    puts "İşlem 3"
else
    "Geçersiz İşlem"
end

=end


print "1. Sayı: "
num1 = gets.to_i

print "2. Sayı: "
num2 = gets.to_f

print "İşlemi Seç: "
option = gets.chomp()

if option == "1"
    puts num1 + num2
elsif option == "2"
    puts num1 - num2
elsif option == "3"
    puts num1 * num2
elsif option == "4"
    puts num1 / num2
else
    puts "Geçersiz işlem"
end