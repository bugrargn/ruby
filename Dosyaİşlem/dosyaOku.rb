file = File.open("mezun.txt", "r")

#Hepsini oku
#puts file.read()

# İlk 10 karakter
#puts file.read(10)

# karakter karakter oku
#puts file.readchar()
#puts file.readchar()

# satır satır
#puts file.readline()
#puts file.readline()

#file.close()

# dosya otomatik kapat
File.open("mezun.txt", "r") do |file|
    for line in file.readlines()
        puts line
        puts "-------------"
    end
end