favorites = []
# push dizi sonuna nesne ekler
favorites.push 'raindrops on roses'
favorites.push 'whiskey on kittens'

puts favorites[0]
# last pop gibi işlem sadece son elemanı bildirir, diziden çıkarmaz
puts favorites.last
puts favorites.length

# pop dizi son elemanı çıkarır ve bildirir
puts favorites.pop
puts favorites
puts favorites.length