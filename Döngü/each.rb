# each dizinin elemanlarının her birinin üstünde tek tek bir uygulamayı yaptırmamızı sağlar. Örneğin dizi içindeki dünya dillerinin her birine iyi bir şeyler demek için : 

diller = ['Türkçe', 'İngilizce', 'Ruby']
diller.each do |dil|
    puts 'Öğreniyorum ' + dil + '!'
    puts 'Değil mi?'
end
puts 'Ve hadi C++\'ı duy'
puts '...'