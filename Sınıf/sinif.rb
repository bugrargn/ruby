class Film
    # sınıf dışından erişime engel için attr_reader,
    # sadece yazma için attr_writer
    # her ikisi için attr_accessor
    # sınıfa 3 özellik oluşturuldu
    attr_accessor :title,:director, :stars

    def initialize(title, director, stars)
        @title = title
        @director = director
        @stars = stars
    end

    def isActorIncluded(actorName)
        isIncluded = false

        for actor in stars
            if (actor == actorName)
                isIncluded = true
            end
        end
        if isIncluded
            puts "Sorguladığınız aktör bu filmde bulunuyor"
        else
            puts "Bu filmde yok"
        end
    end

end

film1 = Film.new("Masumiyet", "Zeki Demirkubuz", ["Haluk Bilginer", "Derya Alabora", "Güven Kıraç"])

puts film1.isActorIncluded("Orlando Bloom")

=begin

film1.title = "Masumiyet"
film1.director = "Zeki Demirkubuz"
film1.stars = ["Haluk Bilginer", "Derya Alabora", "Güven Kıraç"]

=end
